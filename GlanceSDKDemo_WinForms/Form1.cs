﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlanceSDKDemo_WinForms
{
    public partial class Form1 : Form
    {
        static Int32 GLANCE_GROUPID = 15687;        // Replace this with your own Group ID frm Glance

        public Form1()
        {
            InitializeComponent();
            EndSession.Hide();
            Glance.Visitor.OnEvent += HandleGlanceEvent;
            Glance.Visitor.Init(GLANCE_GROUPID, "", "WinForms Demo", "example@example.com", "617-555-6666");
        }

        private void Start_Click(object sender, EventArgs e)
        {
            // Note that StartSession can only be called after EventVisitorInitialized,
            // and it must be called on the main thread
            Glance.Visitor.StartSession();
            Start.Hide();
        }

        private bool HandleGlanceEvent(Glance.Event gevt)
        {
            switch (gevt.code)
            {
                case Glance.EventCode.EventStartSessionFailed:
                    StatusText.Invoke((MethodInvoker)delegate
                    {
                        StatusText.Text = "Could not start session: " + gevt.message;
                        EndSession.Hide();
                        Start.Show();
                    });
                    break;

                case Glance.EventCode.EventConnectedToSession:
                    Key.Invoke((MethodInvoker)delegate
                    {
                        Key.Text = gevt.properties["sessionkey"];
                        StatusText.Text = "Session started - give the key above to the agent";
                        EndSession.Show();
                    });
                    break;

                case Glance.EventCode.EventGuestCountChange:
                    Key.Invoke((MethodInvoker)delegate
                    {
                        StatusText.Text = "Agent connected";
                    });
                    break;

                case Glance.EventCode.EventSessionEnded:
                    StatusText.Invoke((MethodInvoker)delegate
                    {
                        StatusText.Text = "Session ended: " + gevt.message;
                        Key.Text = "";
                        EndSession.Hide();
                        Start.Show();
                    });
                    break;

                default:
                    if (gevt.type == Glance.EventType.EventWarning || gevt.type == Glance.EventType.EventError || gevt.type == Glance.EventType.EventAssertFail)
                    {
                        StatusText.Invoke((MethodInvoker)delegate
                        {
                            StatusText.Text = "Event: " + gevt.code + gevt.message;
                        });
                    }
                    break;
            }
            return true;
        }

        private void EndSession_Click(object sender, EventArgs e)
        {
            Glance.Visitor.EndSession();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
