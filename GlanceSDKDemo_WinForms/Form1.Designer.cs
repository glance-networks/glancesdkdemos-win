﻿namespace GlanceSDKDemo_WinForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Start = new System.Windows.Forms.Button();
            this.Key = new System.Windows.Forms.Label();
            this.EndSession = new System.Windows.Forms.Button();
            this.StatusText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Start
            // 
            this.Start.Location = new System.Drawing.Point(13, 13);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(75, 23);
            this.Start.TabIndex = 0;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // Key
            // 
            this.Key.AutoSize = true;
            this.Key.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Key.Location = new System.Drawing.Point(146, 13);
            this.Key.Name = "Key";
            this.Key.Size = new System.Drawing.Size(0, 20);
            this.Key.TabIndex = 1;
            // 
            // EndSession
            // 
            this.EndSession.Location = new System.Drawing.Point(270, 12);
            this.EndSession.Name = "EndSession";
            this.EndSession.Size = new System.Drawing.Size(75, 23);
            this.EndSession.TabIndex = 2;
            this.EndSession.Text = "End";
            this.EndSession.UseVisualStyleBackColor = true;
            this.EndSession.Click += new System.EventHandler(this.EndSession_Click);
            // 
            // StatusText
            // 
            this.StatusText.Location = new System.Drawing.Point(13, 58);
            this.StatusText.Multiline = true;
            this.StatusText.Name = "StatusText";
            this.StatusText.Size = new System.Drawing.Size(340, 238);
            this.StatusText.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 322);
            this.Controls.Add(this.StatusText);
            this.Controls.Add(this.EndSession);
            this.Controls.Add(this.Key);
            this.Controls.Add(this.Start);
            this.Name = "GlanceSDKDemo_WinForms";
            this.Text = "GlanceSDKDemo_WinForms";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.Label Key;
        private System.Windows.Forms.Button EndSession;
        private System.Windows.Forms.TextBox StatusText;
    }
}

