#pragma once
// (c) 2019 Glance Networks, Inc. - All Rights Reserved
//
//	GlanceVisitor.h - Glance screenshare visitor functionality
//

#include "GlanceDefs.h"

#ifdef ANDROID
#include <string>
#endif

namespace _Glance
{
    /// 
    /// <summary> Static class interface to the single anonymous visitor session. </summary>
    ///
	class glanceapi Visitor
	{
	public:

        /// <summary> Init the Glance Screenshare API.</summary>
        /// <remarks>Call Init once to specify global settings before using the API.
        /// </remarks>
        /// <param name="groupid">The group id</param>
        /// <param name="token">Authorization token for future use.  Pass an empty string</param>
        /// <param name="name">Optional visitor name which will be stored in the with the session record in the Glance database.</param>
        /// <param name="email">Optional visitor email which will be stored in the with the session record in the Glance database.</param>
        /// <param name="phone">Optional visitor phone which will be stored in the with the session record in the Glance database.</param>
        static void Init(int groupid, const GlanceString & token, const GlanceString & name, const GlanceString & email, const GlanceString & phone);

        /// <summary> Init the Glance Screenshare API.</summary>
        /// <remarks>Call Init once to specify global settings before using the API.
        /// </remarks>
        /// <param name="visitorInitParams">The visitor initialization parameters</param>
        static void Init(const VisitorInitParams & visitorInitParams);

        /// <summary>Get a visitor setting as returned by the GetVisitorSettings2 web service</summary>
        /// <remarks>Any of the properties returned by GetVisitorSettings2 can be accessed via this method.
        /// </remarks>
        /// <param name="setting">Name of the visitor setting, case insensitive</param>
        static GlanceString GetVisitorSetting(const GlanceString & setting);

        /// <summary>Release the _Glance::Visitor API </summary>
        /// <remarks>Call this method once when the application is finished using the _Glance::Visitor API, typically on exit.
        ///          The windows dll calls this when the dll unloads.
        /// </remarks>
        static void Release();

		/// <summary>Set an event handler to be called on screenshare session events.</summary>
		/// <param name="eventCallbackFn">The callback function</param>
		static void SetEventHandler(EventHandler eventHandler);

		/// <summary>Start a screenshare session to show the current application.</summary>
		/// <remarks>
		/// </remarks>
		/// <param name="sessionKey">A key to use for the session.  If sessionKey is an empty string, the session is started with a random key.</param>
		static void StartSession(const GlanceString & sessionKey = GlanceString());

		/// <summary>Start a screenshare session with start params</summary>
		/// <remarks>
		/// </remarks>
		/// <param name="sp">Glance StartParams</param>
		static void StartSession(const StartParams & sp);

        /// <summary>   Gets session information. </summary>
        /// <returns>   A SessionInfo object with detailed information about the session. </returns>
        static SessionInfo GetSessionInfo();

		/// <summary>Pause or unpause the screenshare session</summary>
		/// <remarks>While a session is paused, the viewer displays a customizable message instead of the application.
		/// </remarks>
		/// <param name="pause">Pass true to pause the session, false to unpause.</param>
		static void Pause(bool pause);
        
        /// <summary>Freeze or unfreeze the screenshare session</summary>
        /// <remarks>While a session is frozen, the viewer displays the last screenshot sent until unfrozen
        /// </remarks>
        /// <param name="freeze">Pass true to freeze the session, false to unfreeze.</param>
        static void Freeze(bool freeze);

		/// <summary>Enable remote control, so that the agent can control the visitor's application.</summary>
		/// <remarks>Remote control must be enabled for the account.
		/// </remarks>
		/// <param name="enable">True to enable remote control or false to disable it</param>
		static void EnableRC(bool enable);

        /// <summary>   Gets the total number of displays (monitors, devices, webcams) currently connected to the machine. </summary>
        /// <returns>   The display count. </returns>
        static int      GetDisplayCount();

        /// <summary>   Gets the index of the main monitor. </summary>
        /// <returns>   The main monitor index. </returns>
        static int      GetMainMonitor();

        /// <summary>   Draws on each monitor the Display Name that is assigned to that monitor.</summary>
        /// <remarks>   Identifies monitors so that the user knows which is which.</remarks>
        static void     IdentifyMonitors();

        /// <summary>   Gets display type. </summary>
        /// <param name="n">    The index of the display. </param>
        /// <returns>   They type of the display. </returns>
        static DisplayType      GetDisplayType(int n);

        /// <summary>   Gets display name, which can be used to identify a display in the DisplayParams. </summary>
        /// <remarks>    Display name can change as monitors and devices are connected and disconnected.</remarks>
        /// <param name="n">    The index of the display. </param>
        /// <returns>   The display name. </returns>
        static GlanceString     GetDisplayName(int n);

        /// <summary>   Shows the specified display. </summary>
        /// <param name="displayParams">   Parameters indicating which display to show, and how to show it. </param>
        static void     ShowDisplay(const DisplayParams & displayParams);

        /// <summary>
        /// Add an existing session as a child to the currently running screenshare session.
        /// </summary>
        /// <param name="sessionType">Indicates the type of session and determines where the session should be displayed on the agent side,
        ///  e.g. visitorVideo</param>
        /// <param name="sessionKey">The session key</param>
        static void     AddChildSession(const GlanceString & sessionType, const GlanceString & sessionKey, const GlanceString & sessionProperties);

        /// <summary>
        /// Send a message to the agents connected to the session.  The message will
        /// result in a "usermessage" event being fired on the viewer side.
        /// </summary>
        /// <param name="messageName">The name of the message to send</param>
        /// <param name="messageProperties">The properties to send as the body of the message in the form key1:val1;key2:val2</param>
        static void     SendUserMessage(const GlanceString & messageName, const GlanceString & messageProperties);

        /// <summary>
        /// Similar to SendUserMessage, but the message will be retained and re-sent whenever
        /// a new viewer connects.  If SetUserState is called multiple times with the same messageName,
        /// only the data from the most recent call is retained.
        /// </summary>
        /// <param name="messageName">The name of the message to send</param>
        /// <param name="messageProperties">The properties to send as the body of the message in the form key1:val1;key2:val2</param>
        static void     SetUserState(const GlanceString& messageName, const GlanceString& messageProperties);

        /// <summary>Leave the child session with the specified session type.</summary>
        /// <remarks>LeaveChildSession closes the viewer window associated with the specified sessionType</remarks>
        /// <param name="sessionType">The session type, as indicated in the "sessiontype" property of the EventChildSessionStarted event, e.g. "agentVideoHD".</param>
        static void LeaveChildSession(const GlanceString & sessionType);

        /// <summary>Set the viewer context which provides context for the viewer in the event that a session invitation is received.</summary>
        /// <remarks>If the Agent chooses to show video, screen, or device, this viewer context will be used for viewing that session.
        /// If no viewer context is specified, a default viewer will be used.  The Viewer Context must be specified before the video session
        /// begins, and may be specified before StartSession is called.
        /// </remarks>
        /// <param name="viewerContext">A viewer context.</param>
        static void SetVideoViewerContext(ViewerContext * viewerContext);

#if __APPLE__
#if TARGET_OS_IPHONE
		/// <summary>   Sets the list of masked views </summary>
		/// <param name="masked">   NSSet of UIView pointers</param>
		static void SetMaskingContext(MaskingContext * maskingContext);
#else
		/// <summary>   Sets the list of masked views </summary>
		/// <param name="masked">   NSSet of NSView pointers</param>
		static void  SetMaskedViews(void * views);
#endif
#endif

#ifdef WIN32
		/// <summary>   Sets a callback function that supplies a region to be masked </summary>
		static void	SetMaskingCallback(MaskingCallback maskingCallback, void * context);
#endif

#ifdef ANDROID
    	static void ChangeDisplay(int width, int height);
		static void SetOnEventListenerId(std::string id);
        static std::string GetOnEventListenerId();
		static const _Glance::Event & CopyEvent(const _Glance::Event & e);
#endif

		/// <summary>End the screenshare session.</summary>
		/// <remarks>If there is an agent video session running, it will end when the video viewer window
		/// is closed, or the agent turns off the camera.
		/// </remarks>
		static void EndSession();
	};
}
