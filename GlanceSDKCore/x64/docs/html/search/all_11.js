var searchData=
[
  ['sbenabled',['sbEnabled',['../struct___glance_1_1_session_info.html#a0f991849e7d02e2fa463f1aa036ee5cb',1,'_Glance::SessionInfo']]],
  ['scale',['scale',['../struct___glance_1_1_display_params.html#af01f6408bfffc2b27062a23d74ecc347',1,'_Glance::DisplayParams']]],
  ['screenshare',['screenshare',['../struct___glance_1_1_visitor_init_params.html#a9e2941bfc288d31acc1db6631f0dee71',1,'_Glance::VisitorInitParams']]],
  ['sendscreenshareinvitation',['SendScreenshareInvitation',['../class___glance_1_1_glance_session.html#a1d3d1731ae081ddd304276ebfd1eedea',1,'_Glance::GlanceSession']]],
  ['sendusermessage',['SendUserMessage',['../class___glance_1_1_glance_session.html#aae19e7f1d4b94cf93f2a0ed7f979bb56',1,'_Glance::GlanceSession::SendUserMessage()'],['../class___glance_1_1_visitor.html#a112ef7aca7a46aa0b349ab928e4f4ab7',1,'_Glance::Visitor::SendUserMessage()']]],
  ['sessionendedreason',['SessionEndedReason',['../namespace___glance.html#adf1d8fc638d8af134d0042820f42c021',1,'_Glance']]],
  ['sessioninfo',['SessionInfo',['../struct___glance_1_1_session_info.html',1,'_Glance']]],
  ['set',['Set',['../class___glance_1_1_settings.html#abe6a0c38dc6678bbc48a96b2be6941e2',1,'_Glance::Settings']]],
  ['seteventhandler',['SetEventHandler',['../class___glance_1_1_visitor.html#a0b3915a708b1961fc82aad2010f5c822',1,'_Glance::Visitor']]],
  ['settings',['Settings',['../class___glance_1_1_settings.html',1,'_Glance']]],
  ['setuserstate',['SetUserState',['../class___glance_1_1_visitor.html#a03b6c0243b99c031f303e72262fa3f2f',1,'_Glance::Visitor']]],
  ['setvideoviewercontext',['SetVideoViewerContext',['../class___glance_1_1_visitor.html#a8355cfd7c01403cdcd631b85fc72b41c',1,'_Glance::Visitor']]],
  ['show',['show',['../struct___glance_1_1_start_params.html#ab0608fefe5a3c44592d23693438a34fd',1,'_Glance::StartParams::show()'],['../class___glance_1_1_host_session.html#aa48f842c15d79bee82678a376a331eaa',1,'_Glance::HostSession::Show()']]],
  ['showdisplay',['ShowDisplay',['../class___glance_1_1_glance_session.html#a16d5fee4984e263d8862ae8e3444ff59',1,'_Glance::GlanceSession::ShowDisplay()'],['../class___glance_1_1_visitor.html#aee18d41c66c15d2329772e0cae96e11a',1,'_Glance::Visitor::ShowDisplay()']]],
  ['site',['site',['../struct___glance_1_1_visitor_init_params.html#a9e34c2a59895f1aad5464b2d23f56d50',1,'_Glance::VisitorInitParams']]],
  ['startparams',['StartParams',['../struct___glance_1_1_start_params.html',1,'_Glance']]],
  ['startsession',['StartSession',['../class___glance_1_1_visitor.html#a66b5c9c4fcb6f6c5dce42c2e67622131',1,'_Glance::Visitor::StartSession(const GlanceString &amp;sessionKey=GlanceString())'],['../class___glance_1_1_visitor.html#af2f6b86820d66e19a8bba85c0a59c0e5',1,'_Glance::Visitor::StartSession(const StartParams &amp;sp)']]]
];
