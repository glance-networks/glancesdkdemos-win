var searchData=
[
  ['decline',['decline',['../struct___glance_1_1_join_params.html#a43b17c3c2309de0f833fe2b4ff9759a9',1,'_Glance::JoinParams']]],
  ['device',['Device',['../namespace___glance.html#a371096277ebb5c85f80d4ba4029ada37a9d8f1c7cdd51cc93d67e53240ca62326',1,'_Glance']]],
  ['dimviewer',['DimViewer',['../class___glance_1_1_glance_session.html#a330639f30560a5a79b8fcc2752f2a881',1,'_Glance::GlanceSession']]],
  ['displayname',['displayName',['../struct___glance_1_1_session_info.html#a88ecf7e1ade305f19775b795a6461e72',1,'_Glance::SessionInfo::displayName()'],['../struct___glance_1_1_display_params.html#ad3082d4462e6d274b105d7ce83812af9',1,'_Glance::DisplayParams::displayName()']]],
  ['displayparams',['displayParams',['../struct___glance_1_1_start_params.html#af0567bf0663897e2d84875350d2f84d9',1,'_Glance::StartParams']]],
  ['displayparams',['DisplayParams',['../struct___glance_1_1_display_params.html',1,'_Glance']]],
  ['displaytype',['DisplayType',['../namespace___glance.html#a371096277ebb5c85f80d4ba4029ada37',1,'_Glance']]],
  ['drawgesture',['DrawGesture',['../class___glance_1_1_glance_session.html#a56676781d4592fa1e8286c4be1917dd5',1,'_Glance::GlanceSession']]]
];
