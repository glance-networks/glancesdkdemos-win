var searchData=
[
  ['action',['Action',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042',1,'_Glance']]],
  ['actiondisablegestures',['ActionDisableGestures',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a7602ef76f9022eb53cc02edbcb8c406b',1,'_Glance']]],
  ['actiondisableremotecontrol',['ActionDisableRemoteControl',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a0550d9510d140a4bf4eb19dca3b09511',1,'_Glance']]],
  ['actiondisableshowback',['ActionDisableShowback',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a122f8a54bceb907327006f8096498243',1,'_Glance']]],
  ['actionenablegestures',['ActionEnableGestures',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042aaf4828d502473f2d137bd6ba28ef1731',1,'_Glance']]],
  ['actionenableremotecontrol',['ActionEnableRemoteControl',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a1920b55b07cb1fc4f86d1f9bfba9e3ad',1,'_Glance']]],
  ['actionenableshowback',['ActionEnableShowback',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a2eacf22f4bc11fa33597d0d7be6eadf7',1,'_Glance']]],
  ['actionend',['ActionEnd',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042ace4230313078e4a8eef010843ececdd0',1,'_Glance']]],
  ['actionfreeze',['ActionFreeze',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a27f40957667fdbe2df9815d032041d43',1,'_Glance']]],
  ['actionpause',['ActionPause',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a562023ab2f15a5b8f0118f6fc0fc4d5c',1,'_Glance']]],
  ['actionshowdisplay',['ActionShowDisplay',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a64c0945bd9dd21b3a753dc954d0d0995',1,'_Glance']]],
  ['actionstartshow',['ActionStartShow',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a6c668dba65cc2a27fc581a2d763b81c3',1,'_Glance']]],
  ['actionstartview',['ActionStartView',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a49d7f4221b097a562684d1d8b581e0b1',1,'_Glance']]],
  ['actionunfreeze',['ActionUnfreeze',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a894c4b2e7675021a03766527fd9bab54',1,'_Glance']]],
  ['actionunpause',['ActionUnpause',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a2a09dd0e6838de9a9b07e1593078875e',1,'_Glance']]],
  ['addchildsession',['AddChildSession',['../class___glance_1_1_visitor.html#a12b104cf84864e03fc46dab0d7a957a1',1,'_Glance::Visitor']]],
  ['application',['application',['../struct___glance_1_1_display_params.html#ad6111da345212bcf40dc745954c09074',1,'_Glance::DisplayParams']]],
  ['authenticate',['Authenticate',['../class___glance_1_1_user.html#a49d33756c9b512becbc375f495a3a11c',1,'_Glance::User::Authenticate()'],['../class___glance_1_1_user.html#a55c100d739a95a05898cb70389c1e568',1,'_Glance::User::Authenticate(const GlanceString &amp;username, const GlanceString &amp;password)']]],
  ['authenticated',['Authenticated',['../class___glance_1_1_user.html#a2a55bc9c778bc79a26486181e4fd4820',1,'_Glance::User']]],
  ['authenticatewithkey',['AuthenticateWithKey',['../class___glance_1_1_user.html#a7727b57b374cd5a08d52b75a0fb99c34',1,'_Glance::User::AuthenticateWithKey(const GlanceString &amp;username, const GlanceString &amp;loginkey)'],['../class___glance_1_1_user.html#a4bea48121d57acf13787a04c8068a198',1,'_Glance::User::AuthenticateWithKey(int partnerid, const GlanceString &amp;partneruserid, const GlanceString &amp;loginkey)']]]
];
