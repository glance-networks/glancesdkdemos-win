var searchData=
[
  ['instantjoin',['instantJoin',['../struct___glance_1_1_start_params.html#a4d8e6eb70756269155e98fd624a83324',1,'_Glance::StartParams']]],
  ['isconnected',['isConnected',['../struct___glance_1_1_session_info.html#a384ced3cf661bd38d073215cd07f6805',1,'_Glance::SessionInfo']]],
  ['isguest',['isGuest',['../struct___glance_1_1_session_info.html#a38e3caabe2a8eb92cf1f9530c6ad8e6f',1,'_Glance::SessionInfo']]],
  ['ispaused',['isPaused',['../struct___glance_1_1_session_info.html#ae81c34d156c91e268dac2e4d8e6a3b1d',1,'_Glance::SessionInfo']]],
  ['isreverse',['isReverse',['../struct___glance_1_1_session_info.html#a3cb50d89371e6f4c04c3fccdcb9c8ebf',1,'_Glance::SessionInfo']]],
  ['isshowing',['isShowing',['../struct___glance_1_1_session_info.html#a41d1a93fb891afcfdd609af7e4b4bf83',1,'_Glance::SessionInfo']]],
  ['isviewing',['isViewing',['../struct___glance_1_1_session_info.html#a1eaf93e15995d19856227fbcced4b541',1,'_Glance::SessionInfo']]]
];
