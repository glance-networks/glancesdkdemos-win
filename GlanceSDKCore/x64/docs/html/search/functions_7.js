var searchData=
[
  ['identifymonitors',['IdentifyMonitors',['../class___glance_1_1_glance_session.html#af97988e60b8d85b15746b4185dfd02b1',1,'_Glance::GlanceSession::IdentifyMonitors()'],['../class___glance_1_1_visitor.html#a88dda507135cbc1de3f318b8345535de',1,'_Glance::Visitor::IdentifyMonitors()']]],
  ['init',['Init',['../class___glance_1_1_visitor.html#a664a2e5381251c91d8361baf3103cc11',1,'_Glance::Visitor::Init(int groupid, const GlanceString &amp;token, const GlanceString &amp;name, const GlanceString &amp;email, const GlanceString &amp;phone)'],['../class___glance_1_1_visitor.html#a7e8e26d24c113cf1b547c11331c32edd',1,'_Glance::Visitor::Init(const VisitorInitParams &amp;visitorInitParams)']]],
  ['install',['Install',['../class___glance_1_1_settings.html#a2eb85b5c2e3da3fd48c320af8547c50f',1,'_Glance::Settings']]],
  ['invokeaction',['InvokeAction',['../class___glance_1_1_glance_session.html#a708f231971c3072fd1a75c055b54dcb3',1,'_Glance::GlanceSession']]],
  ['isvisitor',['IsVisitor',['../class___glance_1_1_user.html#a9a4ae09b75892c4f80e29fcc1ed4ac84',1,'_Glance::User']]]
];
