var searchData=
[
  ['identifymonitors',['IdentifyMonitors',['../class___glance_1_1_glance_session.html#af97988e60b8d85b15746b4185dfd02b1',1,'_Glance::GlanceSession::IdentifyMonitors()'],['../class___glance_1_1_visitor.html#a88dda507135cbc1de3f318b8345535de',1,'_Glance::Visitor::IdentifyMonitors()']]],
  ['init',['Init',['../class___glance_1_1_visitor.html#a664a2e5381251c91d8361baf3103cc11',1,'_Glance::Visitor::Init(int groupid, const GlanceString &amp;token, const GlanceString &amp;name, const GlanceString &amp;email, const GlanceString &amp;phone)'],['../class___glance_1_1_visitor.html#a7e8e26d24c113cf1b547c11331c32edd',1,'_Glance::Visitor::Init(const VisitorInitParams &amp;visitorInitParams)']]],
  ['install',['Install',['../class___glance_1_1_settings.html#a2eb85b5c2e3da3fd48c320af8547c50f',1,'_Glance::Settings']]],
  ['instantjoin',['instantJoin',['../struct___glance_1_1_start_params.html#a4d8e6eb70756269155e98fd624a83324',1,'_Glance::StartParams']]],
  ['invokeaction',['InvokeAction',['../class___glance_1_1_glance_session.html#a708f231971c3072fd1a75c055b54dcb3',1,'_Glance::GlanceSession']]],
  ['isconnected',['isConnected',['../struct___glance_1_1_session_info.html#a384ced3cf661bd38d073215cd07f6805',1,'_Glance::SessionInfo']]],
  ['isguest',['isGuest',['../struct___glance_1_1_session_info.html#a38e3caabe2a8eb92cf1f9530c6ad8e6f',1,'_Glance::SessionInfo']]],
  ['ispaused',['isPaused',['../struct___glance_1_1_session_info.html#ae81c34d156c91e268dac2e4d8e6a3b1d',1,'_Glance::SessionInfo']]],
  ['isreverse',['isReverse',['../struct___glance_1_1_session_info.html#a3cb50d89371e6f4c04c3fccdcb9c8ebf',1,'_Glance::SessionInfo']]],
  ['isshowing',['isShowing',['../struct___glance_1_1_session_info.html#a41d1a93fb891afcfdd609af7e4b4bf83',1,'_Glance::SessionInfo']]],
  ['isviewing',['isViewing',['../struct___glance_1_1_session_info.html#a1eaf93e15995d19856227fbcced4b541',1,'_Glance::SessionInfo']]],
  ['isvisitor',['IsVisitor',['../class___glance_1_1_user.html#a9a4ae09b75892c4f80e29fcc1ed4ac84',1,'_Glance::User']]]
];
