var searchData=
[
  ['sendscreenshareinvitation',['SendScreenshareInvitation',['../class___glance_1_1_glance_session.html#a1d3d1731ae081ddd304276ebfd1eedea',1,'_Glance::GlanceSession']]],
  ['sendusermessage',['SendUserMessage',['../class___glance_1_1_glance_session.html#aae19e7f1d4b94cf93f2a0ed7f979bb56',1,'_Glance::GlanceSession::SendUserMessage()'],['../class___glance_1_1_visitor.html#a112ef7aca7a46aa0b349ab928e4f4ab7',1,'_Glance::Visitor::SendUserMessage()']]],
  ['set',['Set',['../class___glance_1_1_settings.html#abe6a0c38dc6678bbc48a96b2be6941e2',1,'_Glance::Settings']]],
  ['seteventhandler',['SetEventHandler',['../class___glance_1_1_visitor.html#a0b3915a708b1961fc82aad2010f5c822',1,'_Glance::Visitor']]],
  ['setuserstate',['SetUserState',['../class___glance_1_1_visitor.html#a03b6c0243b99c031f303e72262fa3f2f',1,'_Glance::Visitor']]],
  ['setvideoviewercontext',['SetVideoViewerContext',['../class___glance_1_1_visitor.html#a8355cfd7c01403cdcd631b85fc72b41c',1,'_Glance::Visitor']]],
  ['show',['Show',['../class___glance_1_1_host_session.html#aa48f842c15d79bee82678a376a331eaa',1,'_Glance::HostSession']]],
  ['showdisplay',['ShowDisplay',['../class___glance_1_1_glance_session.html#a16d5fee4984e263d8862ae8e3444ff59',1,'_Glance::GlanceSession::ShowDisplay()'],['../class___glance_1_1_visitor.html#aee18d41c66c15d2329772e0cae96e11a',1,'_Glance::Visitor::ShowDisplay()']]],
  ['startsession',['StartSession',['../class___glance_1_1_visitor.html#a66b5c9c4fcb6f6c5dce42c2e67622131',1,'_Glance::Visitor::StartSession(const GlanceString &amp;sessionKey=GlanceString())'],['../class___glance_1_1_visitor.html#af2f6b86820d66e19a8bba85c0a59c0e5',1,'_Glance::Visitor::StartSession(const StartParams &amp;sp)']]]
];
