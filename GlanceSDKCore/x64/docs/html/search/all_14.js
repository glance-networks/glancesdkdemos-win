var searchData=
[
  ['video',['video',['../struct___glance_1_1_display_params.html#ab9cc79c7a66611be888aa59f4263e526',1,'_Glance::DisplayParams::video()'],['../struct___glance_1_1_start_params.html#a8a807043ff318a1b9cdbca16f41382d2',1,'_Glance::StartParams::video()'],['../struct___glance_1_1_visitor_init_params.html#ad2b77fa3256f60f09418dd1bdda4e57d',1,'_Glance::VisitorInitParams::video()']]],
  ['view',['View',['../class___glance_1_1_host_session.html#acb4e1bcd403844881ce74af4290de0ee',1,'_Glance::HostSession']]],
  ['viewercloseable',['viewerCloseable',['../struct___glance_1_1_start_params.html#abf656da9c236a82ebc8e43e64e038916',1,'_Glance::StartParams::viewerCloseable()'],['../struct___glance_1_1_join_params.html#ae6f53d9085dda4c0ad0fd54c7f7ed742',1,'_Glance::JoinParams::viewerCloseable()']]],
  ['visitor',['Visitor',['../class___glance_1_1_visitor.html',1,'_Glance']]],
  ['visitorid',['visitorid',['../struct___glance_1_1_visitor_init_params.html#a3287b3360d427c3c7a07834d3a432e79',1,'_Glance::VisitorInitParams']]],
  ['visitorinitparams',['VisitorInitParams',['../struct___glance_1_1_visitor_init_params.html',1,'_Glance']]]
];
