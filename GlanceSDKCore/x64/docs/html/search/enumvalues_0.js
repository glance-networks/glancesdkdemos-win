var searchData=
[
  ['actiondisablegestures',['ActionDisableGestures',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a7602ef76f9022eb53cc02edbcb8c406b',1,'_Glance']]],
  ['actiondisableremotecontrol',['ActionDisableRemoteControl',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a0550d9510d140a4bf4eb19dca3b09511',1,'_Glance']]],
  ['actiondisableshowback',['ActionDisableShowback',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a122f8a54bceb907327006f8096498243',1,'_Glance']]],
  ['actionenablegestures',['ActionEnableGestures',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042aaf4828d502473f2d137bd6ba28ef1731',1,'_Glance']]],
  ['actionenableremotecontrol',['ActionEnableRemoteControl',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a1920b55b07cb1fc4f86d1f9bfba9e3ad',1,'_Glance']]],
  ['actionenableshowback',['ActionEnableShowback',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a2eacf22f4bc11fa33597d0d7be6eadf7',1,'_Glance']]],
  ['actionend',['ActionEnd',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042ace4230313078e4a8eef010843ececdd0',1,'_Glance']]],
  ['actionfreeze',['ActionFreeze',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a27f40957667fdbe2df9815d032041d43',1,'_Glance']]],
  ['actionpause',['ActionPause',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a562023ab2f15a5b8f0118f6fc0fc4d5c',1,'_Glance']]],
  ['actionshowdisplay',['ActionShowDisplay',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a64c0945bd9dd21b3a753dc954d0d0995',1,'_Glance']]],
  ['actionstartshow',['ActionStartShow',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a6c668dba65cc2a27fc581a2d763b81c3',1,'_Glance']]],
  ['actionstartview',['ActionStartView',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a49d7f4221b097a562684d1d8b581e0b1',1,'_Glance']]],
  ['actionunfreeze',['ActionUnfreeze',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a894c4b2e7675021a03766527fd9bab54',1,'_Glance']]],
  ['actionunpause',['ActionUnpause',['../namespace___glance.html#a7044d3a70d8c23d1e8a910d07a652042a2a09dd0e6838de9a9b07e1593078875e',1,'_Glance']]]
];
