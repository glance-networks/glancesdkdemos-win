#pragma once
// (c) 2017-2019 Glance Networks, Inc. - All Rights Reserved
//
//	GlanceDefs.h - Common definitions
//

// NULL
/* Define NULL pointer value */
#ifdef WIN32
#ifndef NULL
#define NULL    nullptr
#endif
#endif

#ifdef ANDROID
#ifndef NULL
#define NULL    nullptr
#endif
#endif

// Dll Exports

#ifdef WIN32
#ifdef GLANCEDLL_EXPORT
#define glanceapi _declspec(dllexport)
#else
#define glanceapi _declspec(dllimport)
#endif
#else
#define glanceapi 
#endif

#ifdef WIN32
#ifdef _UNICODE
typedef const wchar_t * NativeStrPtr;
#else
typedef const char * NativeStrPtr;
#endif
typedef bool(__stdcall *MaskingCallback)(void*, void *);
#elif __APPLE__
typedef CFStringRef NativeStrPtr;
#elif ANDROID
typedef const char * NativeStrPtr;
#else
typedef const char * NativeStrPtr;
#endif

#if GIOS
OBJC_CLASS(GlanceCustomViewerDelegate);
#endif

class GStr;
class GlanceSessionInternal;
class AnonymousHostSession;
class VisitorSession;

namespace _Glance
{
    class Event;

	///
	/// <summary>   String class for strings passed in and out of the Glance client API. </summary>
	///
	class glanceapi GlanceString
	{
	public:
		/// <summary>   Default constructor instantiates an empty GlanceString. </summary>
		GlanceString();

		/// <summary>   Copy constructor. </summary>
		GlanceString(const GlanceString & s);

		/// <summary>   Construct an instance of GlanceString from a native C string pointer. </summary>
		GlanceString(NativeStrPtr s);

		/// <summary>   Destructor. </summary>
		~GlanceString();

		/// <summary>   Cast an instance of GlanceString to a native C string pointer. </summary>
		operator NativeStrPtr () const;

		GlanceString & operator=(const GlanceString &);

	private:
		friend class Settings;
		friend class User;
		friend class Event;
		friend class GlanceSession;
		friend class HostSession;
		friend class GuestSession;
		friend class ::GlanceSessionInternal;
		friend class EventQueue;
		friend class Visitor;
        friend class ::VisitorSession;
		friend class ::AnonymousHostSession;

		GStr *  gstr;
	};

	///
	/// <summary>   Code identifying a particular Glance Event. </summary>
	///
	enum EventCode
	{
		EventNone
		, EventInvalidParameter      ///< An invalid input parameter was passed into an API method
		, EventInvalidState          ///< There was an attempt to invoke an Action that was not valid in current state
		, EventLoginSucceeded        ///< User login succeeded
		, EventLoginFailed           ///< User login failed
		, EventPrivilegeViolation    ///< There was an attempt to do something that is not permitted by the User's privileges
		, EventInvalidWebserver      ///< Attempt to direct the client to connect to a webserver that the client has not been configured to allow
		, EventUpgradeAvailable      ///< An upgrade to the client library is available
		, EventUpgradeRequired       ///< An upgrade to the client library is required
		, EventCompositionDisabled   ///< Desktop composition was disabled (Windows Vista only)
		, EventConnectedToSession    ///< Client successfully connected to a session, or reconnected after changing roles (display to viewer or vice versa)
		, EventSwitchingToView       ///< A viewer participant in the session has started showing his screen, so the display is switching to view
		, EventStartSessionFailed    ///< An attempt to start a new session was unsuccessful
		, EventJoinFailed            ///< An attempt to join a session was unsuccessful
		, EventSessionEnded          ///< Session endded
		, EventFirstGuestSession     ///< A guest's first-ever session just ended, prompt user to uninstall 
		, EventTunneling             ///< Connection is tunneling
		, EventRestartRequired       ///< To complete the driver install, a machine restart is required
		, EventConnectionWarning     ///< Connectivity has been lost
		, EventClearWarning          ///< Connectivity has been restored
		, EventGuestCountChange      ///< The guest count changed
		, EventViewerClose           ///< User clicked the viewer window close button
		, EventActionsChange         ///< The set of allowed actions changed (NOT IMPLEMENTED)
		, EventDriverInstallError    ///< There was an error installing Glance SpeedBoost.
		, EventRCDisabled            ///< Remote control has been disabled.
		, EventDeviceDisconnected    ///< Captured (video) device disconnected.  Can be reconnected and session continued.
		, EventDeviceReconnected     ///< Captured (video) device reconnected. 
		, EventGesture               ///< A gesture was received.
		, EventException             ///< Unexpected exception 
		, EventScreenshareInvitation ///< A screenshare session invitation received from another session participant
		, EventMessageReceived       ///< A user message was received from another session participant
        , EventChildSessionStarted   ///< A child session started.
        , EventChildSessionEnded     ///< A child session ended.
        , EventJoinChildSessionFailed///< An invitation was received to join a child session was received but the session could not be found
        , EventVisitorInitialized    ///< Visitor API successfully initialized
        , EventPresenceConnected     ///< Visitor connected to presence service
        , EventPresenceConnectFail   ///< Visitor failed to connect to presence service
        , EventPresenceShowTerms     ///< The agent signaled the visitor to show terms and conditions
        , EventPresenceStartSession  ///< The agent signaled the visitor to start a session
        , EventPresenceSignal        ///< The agent signaled the visitor 
        , EventPresenceBlur          ///< Another application connected to presence with the same visitor id
        , EventPresenceSendFail      ///< Failed to send a message on the presence connection due to connection drop
        , EventPresenceNotConfigured ///< Presence is not configured or not enabled for the group
        , EventPresenceDisconnected  ///< The application has disconnected from the presence service
        , EventPresenceStartVideo    ///< The agent signaled the visitor to start a video session
	};

	/// 
	/// <summary>   Type of event. </summary>
	///
	enum EventType
	{
		EventTypeNone
		, EventInfo      ///< An event of possible interest to the user
		, EventWarning   ///< A possibly temporary problem is occurring, e.g. network connectivity has been lost
		, EventError     ///< A permanent error has occurred 
		, EventAssertFail///< A coding error, e.g. an invalid parameter was passed in.
	};

	// Event Properties

	/// 
	/// <summary>   EventSessionEnded "reason" property values  </summary>
	///
	enum SessionEndedReason
	{
		SessionEndReasonNone,
		SessionEndReasonThisSide,           // Session ended intentionally on this side
		SessionEndReasonOtherSide,          // Session ended by other side
		SessionEndReasonCantConnect,        // Unable to connect to the session server
		SessionEndReasonConnDrop,           // Connection dropped and could not be reestablished
		SessionEndReasonKilled,             // Session was killed
		SessionEndReasonHasDisplay,         // Two guests tried to join reverse session simultaneously
		SessionEndReasonDeclined,           // Guest in a reverse session declined
		SessionEndTimedout,                 // Timed out waiting for guest to join
		SessionEndReplaced,                 // A new viewer was open by the same guest, replacing this one
		SessionEndReasonError               // An unexpected error occurred.
	};

	/// 
	/// <summary>Callback function which is called with an event.</summary>
	///
	typedef void (* EventHandler) (const Event & event);
}
