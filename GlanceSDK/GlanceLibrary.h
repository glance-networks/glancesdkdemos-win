// (c) 2014-2019 Glance Networks, Inc. - All Rights Reserved
//
//	GlanceLibrary.h - Glance Client Exported Library Functions
//

#pragma once
#include "GlanceDefs.h"
#include "GPromise.h"

enum ViewerContextContentMode {ScaleAspectFill, ScaleAspectFit, ScaleToFill};

struct ViewerContext {
	ViewerContext() : customViewerDelegate(nullptr), maxWindowSizeWidth(0), maxWindowSizeHeight(0), contentMode(ScaleToFill), windowTopmost(false) {}
	ViewerContext(void * d, int w, int h, ViewerContextContentMode cm, bool t) : customViewerDelegate(d), maxWindowSizeWidth(w), maxWindowSizeHeight(h), contentMode(cm), windowTopmost(t) {}

    void * customViewerDelegate;
    int maxWindowSizeWidth;
    int maxWindowSizeHeight;
    ViewerContextContentMode contentMode;
	bool windowTopmost;
};

struct MaskingContext {
    void * maskedViews;
    void * maskedLabels;
    bool keyboard;
    void * keyboardLabel;
#if GIOS
    void * webViewMasks;
    bool disableRotation;
    bool visitorPaused;
    ulong lastFreezeAt;
#endif
};

class EventInternal;
class UserInternal;
class GlanceSessionInternal;
class AnonymousHostSession;
class VisitorSession;

///
/// <summary>   _Glance namespace. </summary>
///
namespace _Glance
{
/// 
/// <summary>   Represents an event that occurred. </summary>
/// <remarks>An event could indicate the result of an API method call, or something that ocurred during the course of a screensharing session.</remarks>
///
class glanceapi Event
{
public:
    ~Event();

    EventCode       code;   ///< Identifies the event that occurred
    EventType       type;   ///< The category of event indicating how it should be reported
    GlanceString    message;///< A localized friendly text message describing the event

    /// <summary>   Get the value of an event attribute. </summary>
    /// <returns>   The attribute value.</returns>
    GlanceString    GetValue(const GlanceString & attribute) const;

    /// <summary>   Get the name of attribute n. </summary>
    /// <returns>   The attribute name, or empty string if n is out of bounds.</returns>
    GlanceString    GetAttribute(int n) const;

protected:
    // Events are created internally to the library only
    friend class ::UserInternal;
    friend class ::GlanceSessionInternal;
    friend class Settings;
    friend class EventQueue;
    friend class User;
    friend class GlanceSession;
    friend class HostSession;
    friend class GuestSession;
    friend class Screenshare;
    friend class ::AnonymousHostSession;
    friend class ::VisitorSession;
    friend class Visitor;
    friend class PresenceThread;

    Event(EventCode c, EventType t, const GlanceString & m);
    Event(const Event &); // prevent inadvertent copy
    void CopyFrom(const Event &);

private:
    EventInternal *     _ei;
};

/// 
/// <summary>   Information about a session in progress. </summary>
///
struct SessionInfo   
{ 
    unsigned long   callId;				///< The id of this call.  Pass this value as mainCallId when starting a session that is a subsession of this session.
    GlanceString    offerURL;           ///< Offer url for video sessions
    int             nGuests;			///< Number of guests currently in the session
    GlanceString    glanceAddress;		///< Glance address of session host
    GlanceString    key;				///< Session key
    GlanceString    hostName;			///< First and last name of session host
	bool			rcRequested;		///< True if remote control will be enabled when guest joins the session.  Can be true for reverse sessions only.
    bool            isGuest;			///< True if in the session as a guest
    bool            isReverse;			///< True if a reverse session
	bool            rcEnabled;			///< True if remote control is currently enabled
    bool            sbEnabled;			///< True if showback is currently enabled
    bool            gesturesEnabled;	///< True if gestures are currently enabled
	bool            isPaused;			///< True if the session is paused (host side only)
    bool            isConnected;		///< True if the participant is currently connected
    bool            isShowing;			///< True if showing, false if viewing
    bool            isViewing;			///< True if viewing, false if showing
    GlanceString    displayName;		///< Name of the display currently being shown (host side only)
}; 

typedef struct {
	#ifdef WIN32
    struct HWND__ * window;
    #else
    long            window;
    #endif
	unsigned long	process;
	GlanceString    names;
} _AppSelector;

/// 
/// <summary>   Specifies information about a monitor or device to be displayed. </summary>
///
struct glanceapi DisplayParams 
{ 
    DisplayParams();

    GlanceString    displayName;    ///< The DisplayName of one of the displays currently connected to the machine.  "Main" or blank indicates "primary"
    float           scale;          ///< Scale factor for scaling the sceen image on the display side
    int             captureWidth;   ///< Width of the display to capture (0 for full width)
    int             captureHeight;  ///< Height of the display to capture (0 for full height)
    bool            video;          ///< Use video mode, valid for displays of type Device only
	_AppSelector	application;	///< Show application or window (and sub-windows) only.  Pass "Process" or "Window" in displayName
};

/// 
/// <summary>   Flags indicating what guest information fields should be required or hidden on the join for for the session. </summary>
///
enum GuestInfoFlags
{
    GuestNameRequired = 0x0200  ///< Name is required
    ,GuestEmailRequired = 0x0400///< Email address is required
    ,GuestPhoneRequired = 0x0800///< Phone number is required
    ,GuestNameHidden = 0x1000   ///< Name field is hidden
    ,GuestEmailHidden = 0x2000  ///< Email address field is hidden
    ,GuestPhoneHidden = 0x4000  ///< Phone number field is hidden
};

/// 
/// <summary>   Specifies information about a new session to be started. </summary>
///
struct glanceapi StartParams     
{ 
    StartParams();

    unsigned long   mainCallId;     ///< The call id of the main session if this is a subsession
    short           maxGuests;      ///< Guest count limit
    bool            show;           ///< True if starting a session to show the screen, false for view
    unsigned long   guestInfoFlags; ///< Flags indicating what guest information is collected and/or required
    bool            encrypt;        ///< true if the session (both guest and host side) should be encrypted, false otherwise
    GlanceString    key;            ///< The session key
    DisplayParams   displayParams;  ///< Parameters that specify the display to be shown when the session starts
    bool            requestRC;      ///< Set to True if the host wants remote control in a reverse session
    bool            instantJoin;    ///< Set to True if guests should join with the Instant viewer
    bool            forceTunnel;    ///< Use a tunneling protocol
    bool            viewerCloseable;///< true if the user should be able to close the viewer window directly, false to get an EventViewerClose message instead
    bool            reportErrors;   ///< true if the library should report errors to glance
	bool			persist;		///< Session should persist until host ends (or timeout, similar to waiting)
    bool            paused;         ///< Session should start in a paused (hidden) state.  Applies to "show" sessions
    bool            presenceStart;  ///< Session is being started via presence invoke
};

///
/// <summary> Information for initializing the Visitor API </summary>
///
struct glanceapi VisitorInitParams
{
    VisitorInitParams(int groupid);

    int groupid;        ///< The group id
    GlanceString site;  ///< "staging" or "production"
    GlanceString token; ///< Reserved for future use
    GlanceString visitorid; ///< Visitor id (required for presence)
    GlanceString name;  ///< Visitor name (optional)
    GlanceString email; ///< Visitor email (optional)
    GlanceString phone; ///< Visitor phone (optional)
    bool screenshare; ///<Application supports starting screenshare via presence
    bool video; ///<Application supports starting video via presence
    GlanceString cameras; ///<Comma separated list of supported cameras, e.g. front,back
};

/// 
/// <summary>   Specifies information about a session to be joined. </summary>
///
struct glanceapi JoinParams    
{
    JoinParams();

    unsigned long   guestID;        ///< Guest Id.  Zero to allow Glance to assign a random guest id (recommended).
    bool            decline;        ///< Join the session only for the purpose of notifying host that the guest declined
    GlanceString    guestName;      ///< Guest's name
    GlanceString    guestPhone;     ///< Guest's phone number
    GlanceString    guestEmail;     ///< Guest's email address
    bool            forceTunnel;    ///< Use a tunneling protocol
    bool            viewerCloseable;///< true if the user should be able to close the viewer window directly, false to get an EventViewerClose message instead 
    bool            reportErrors;   ///< true if the library should report errors to glance
    bool            useNETServices; ///< Use the .NET based JoinSession service.
};

/// 
/// <summary>   Actions that can be taken during a session. </summary>
///
enum  Action      
{
    ActionNone
	,ActionStartShow                ///< Start a session to show screen
	,ActionStartView                ///< Start a session to view screen
    ,ActionShowDisplay              ///< Show a different screen or device
    ,ActionPause                    ///< Pause a session which is currently showing.
    ,ActionUnpause                  ///< Unpause a session
    ,ActionFreeze                   ///< Freeze a session which is currently showing.
    ,ActionUnfreeze                 ///< Unfreeze a session
    ,ActionEnableRemoteControl      ///< Enable remote control, allowing other side to control screen
    ,ActionDisableRemoteControl     ///< Disable remote control
    ,ActionEnableShowback           ///< Enable showback for the session, allowing guests to show their screens
    ,ActionDisableShowback          ///< Disable showback
	,ActionEnableGestures           ///< Enable gesturing for the session, allowing guests to draw on the display side screen
	,ActionDisableGestures          ///< Disable gesturing
    ,ActionEnd                      ///< End the session
};

class glanceapi OnEventListener {
public:
    virtual ~OnEventListener() {}
    virtual void    OnEvent(const Event &) = 0;
};

class glanceapi _GlanceBase
{
public:
    virtual void    OnEvent(const Event &e) {
        if (listener != 0) {
            listener->OnEvent(e);
        }
    }

    static void setOnEventListener(OnEventListener *l) {
        listener = l;
    }

protected:
    _GlanceBase();
    virtual ~_GlanceBase();
    
private:
    static OnEventListener *listener;
};

///
/// <summary>   Storing and retrieval of Glance client configuration settings. </summary>
///
class glanceapi Settings : public _GlanceBase
{
public:
    ///
    /// <summary>  Install the library. </summary>
    /// <remarks>  The installation process completes in the background.  On Windows, this installs the Glance 
    ///            SpeedBoost mirror driver. Call CheckInstall to determine if the install was successful.
    /// </remarks>
    void            Install();

    ///
    /// <summary>   Uninstall the library.  After calling this method, a reboot is required to complete the
    ///              uninstall process.
    /// </summary>
    void            Uninstall();

    enum DriverStatus 
    { 
        DriverNotInstalled, 
        DriverInstallInProgress,
        DriverInstalledOK, 
        DriverInstallRebootRequired,
        DriverInstallFailed
    };

    /// <summary>   Check the library install.</summary>
    /// <remarks>   The most likely causeof a install failure on Windows is that the Glance SpeedBoost mirror driver 
    ///             failed to install because it was previously installed and uninstalled.  Instruct the user to log 
    ///             out and try again.
    /// </remarks>
    /// <returns>   DriverStatus
    /// </returns>
    DriverStatus    CheckInstall();

    /// <summary>   Set a configuration value. </summary>
    /// <remarks>   Configuration settings (case sensitive) include: <br/>
    ///             "GlanceAddress" <br/>
    ///             "password" <br/>
    ///             "ForceAJAX" <br/>
    ///             "Encrypt" <br/>
    ///             "EnableShowback" <br/>
	///             "EnableGestures" <br/>
    ///             "GIFlags" <br/>
    ///             "SessionKey"
    /// </remarks>
    /// <param name="name">     The name of the configuration setting. </param>
    /// <param name="value">    The value. </param>
    void            Set(const GlanceString & name, const GlanceString & value);

    /// <summary>   Get a configuration value. </summary>
    /// <param name="name"> The name of the configuration setting to get. </param>
    /// <returns>   The value of the configuration setting. </returns>
    GlanceString    Get(const GlanceString & name);
};

///
/// <summary>   Credentials, typically for web services. </summary>
///
struct glanceapi GlanceCredentials
{
    GlanceCredentials();
    
    GlanceString    username;       ///< Username, e.g. mary.glance.net
    unsigned long   partnerID;      ///< partner's id
    GlanceString    partnerUserID;  ///< partner's id for this user
    GlanceString    loginKey;       ///< time limited authentication token
    GlanceString    gssnid;         ///< login session id
};
        
///
/// <summary>   Represents a Glance User. </summary>
///
class glanceapi User : public _GlanceBase
{
public:

    /// <summary>   Default constructor. </summary>
    User();

    /// <summary>   Destructor. </summary>
    ~User();

    /// <summary>   Authenticates the user using credentials that were stored using Settings. </summary>
    /// <remarks>   Authenticate() uses the "GlanceAddress" and "password" that have been stored in Settings to authenticate the User.
    ///             Authentication completes asynchronously.  Either EventLoginSucceeded or EventLoginFailed will be fired.</remarks>
    void            Authenticate(); 

    /// <summary>   Authenticates the user using the supplied username and password. </summary>
    /// <remarks>   Authentication completes asynchronously.  Either EventLoginSucceeded or EventLoginFailed will be fired. </remarks> 
    /// <param name="username"> The username. </param>
    /// <param name="password"> The password. </param>
    void            Authenticate(const GlanceString & username, const GlanceString & password);

    /// <summary>   Authenticates the user using the supplied username and login key. </summary>
    /// <remarks>   Authentication completes asynchronously.  Either EventLoginSucceeded or EventLoginFailed will be fired.</remarks>
    /// <param name="username"> The username. </param>
    /// <param name="loginkey"> The loginkey. </param>
    void            AuthenticateWithKey(const GlanceString & username, const GlanceString & loginkey);

    /// <summary>   Authenticates the user using the supplied partner id, partner user id, and login key. </summary>
    /// <remarks>   Authentication completes asynchronously.  Either EventLoginSucceeded or EventLoginFailed will be fired.</remarks>
    /// <param name="partnerid"> The partner id. </param>
    /// <param name="partneruserid"> The partner user id. </param>
    /// <param name="loginkey"> The loginkey. </param>
    void            AuthenticateWithKey(int partnerid, const GlanceString & partneruserid, const GlanceString & loginkey);

    /// <summary>   Determines if the user has been authenticated. </summary>
    /// <remarks>   A user must be authenticated in order to start a session which can be joined by unauthenticated guests.</remarks>
    /// <returns>   true if the user is authenticated, false otherwise </returns>
	bool		    Authenticated();

    /// <summary>   Returns a login session id valid for the given number of seconds </summary>
    /// <remarks>   Checks if existing session is valid, may re-authenticate.</remarks>
    /// <returns>   Session id string </returns>
    GlanceString    GetGssnid(int validSeconds, GlanceString & err);
    
    /// <summary>   Returns authorization credentials valid for the given number of seconds </summary>
    /// <returns>   true if valid credentials are returned </returns>
    bool            GetCredentials(int validSeconds, GlanceCredentials & credentials, GlanceString & err);

	/// <summary>   Determines if the user is an anonymous visitor. </summary>
	/// <returns>   true if the user is anonymous, false otherwise </returns>
	bool            IsVisitor();

    /// <summary>   Logs the user out. </summary>
    /// <remarks>   Ends the user's login session.</remarks>
    void            Logout();

    /// <summary>   Get account setting value as a string </summary>
    /// <param name="name"> The setting name. </param>
    /// <remarks>   The setting name may be one of the following values:</remarks>
    /// <list type="bullet">
    /// <item>"UID": Glance user id</item>
    /// <item>"Status": Login status (OK, )</item>
    /// <item>"Msg": Informational message to display</item>
    /// <item>"LinkText": Text of link that accompanies informational messsage</item>
    /// <item>"LinkURL": URL for link that accompanies informational message</item>
    /// <item>"PlanTitle": Name of the Glance plan associated with the user's subscription</item>
    /// <item>"PlanTimeout": Seconds until subscription expires</item>
    /// <item>"IsTrial": 1 if the subscription is a trial, 0 otherwise</item>
    /// <item>"IsBeta": 1 if the client is a beta version, 0 otherwise</item>
    /// <item>"Timeout": Seconds that the session will wait for a guest to join before automatically ending</item>
    /// <item>"rc" : can enable remote control (on/off)</item>
    /// <item>"keyless": can host a keyless session (on/off)</item>
    /// <item>"giflags": can override group settings regarding required guest information (on/off)</item>
    /// <item>"sb": can enable showback (on/off)</item>
    /// <item>"encrypt": can require all connections be encrypted (on/off)</item>
    /// <item>"guests": guest count limit</item>
    /// </list>
    /// 
    /// <returns>   The setting value. </returns>
    GlanceString    GetAccountSetting(const GlanceString & name);

private:
    /// <summary>   Initialize the user as an anonymous visitor.</summary>
    /// <remarks>   Only authenticated guests can join a session that was started by an anonymous visitor.</remarks>
    GPromise<bool>  VisitorIdentify(int groupid, const GlanceString & site,
                                    const GlanceString & visitorid,
                                    const GlanceString & name,
                                    const GlanceString & email,
                                    const GlanceString & phone,
                                    bool screenshare,
                                    bool video,
                                    const GlanceString & cameras);
    
    friend class    GlanceSession;
    friend class    Visitor;
    UserInternal *  _ui;
};

/// 
/// <summary>Type of display</summary>
///
enum DisplayType 
{ 
    Monitor ///< A monitor (screen)
    ,Device ///< A device with video input (e.g. webcam, ipad, etc)
};

/// <summary>   Represents a Glance screeshare session. </summary>
class glanceapi GlanceSession : public _GlanceBase
{
public:
    /// <summary>   Gets the total number of displays (monitors, devices, webcams) currently connected to the machine. </summary>
    /// <returns>   The display count. </returns>
    static int              GetDisplayCount();

    /// <summary>   Gets the index of the main monitor. </summary>
    /// <returns>   The main monitor index. </returns>
    static int              GetMainMonitor();

    /// <summary>   Draws on each monitor the Display Name that is assigned to that monitor.</summary>
    /// <remarks>    Identifies monitors so that the user knows which is which.</remarks>
    static void             IdentifyMonitors(); 

    /// <summary>   Gets display type. </summary>
    /// <param name="n">    The index of the display. </param>
    /// <returns>   They type of the display. </returns>
    static DisplayType      GetDisplayType(int n);

    /// <summary>   Gets display name, which can be used to identify a display in the DisplayParams. </summary>
    /// <remarks>    Display name can change as monitors and devices are connected and disconnected.</remarks>
    /// <param name="n">    The index of the display. </param>
    /// <returns>   The display name. </returns>
    static GlanceString     GetDisplayName(int n);

    /// <summary>   Determine if the action can be invoked on the session at this time. </summary>
    /// <param name="action">   The Action. </param>
    /// <returns>   true if the action can currently be invoked, false if not. </returns>
	bool		            CanInvokeAction(Action action);

    /// <summary>   Invokes an Action on the session. </summary>
    /// <param name="action">   The action to invoke. </param>
    void                    InvokeAction(Action action);

    /// <summary>   Gets session information. </summary>
    /// <returns>   A SessionInfo object with detailed information about the session. </returns>
    SessionInfo             GetSessionInfo();

    /// <summary>   Dims the viewer. </summary>
    /// <remarks>   This method should be called when a message box or dialog needs to be displayed over a session viewer, to distinguish
    ///             the message box from the screen image displayed in the viewer.  Otherwise, the message will appear to be
	///				part of the display side's screen.
	/// </remarks>
    /// <param name="dim">  true to dim. </param>

    void                    DimViewer(bool dim);

    /// <summary>   Draws a gesture on the screen currently being shown. </summary>
    /// <remarks>   DrawGesture can only be called when showing a screen.  The gesture is drawn at the specified coordinates
    ///             and can optionally fade.  Gestures drawn by the viewer are handled automatically.  Use DrawGesture for 
	///				gestures initiated on the display side.
	/// </remarks>
    /// <param name="x">  x coordinate of top left corner of the gesture </param>
	/// <param name="y">  y coordinate of top left corner of the gesture </param>
	/// <param name="width">  width of the gesture </param>
	/// <param name="height"> height of the gesture </param>
	void                    DrawGesture(int x, int y, int width, int height, bool fade);

    /// <summary>   Send a message to other participants in the session</summary>
    /// <remarks>   A message from a "show" participant will be received by all "view" participants
    ///             and vice versa.  An EventMessageReceived event will fire on the receiving side,
    ///             with the data in the message passed through as event properties.
    /// </remarks>
    /// <param name="message">key/value pairs in the format key1:val1;key2:val2; The maximum message length
    ///                       is 4k characters.
    /// </param>
    void                    SendUserMessage(const GlanceString & message);

    /// <summary>   Invite the other participants in the session to join a screenshare session.</summary>
    /// <remarks>   If called by a Display participant, all connected Viewers are invited to join.
    ///             If called by a Viewer participant, the Display is invited to join.  An EventScreenshareInvitation
    ///             event will fire on the recipient side.
    /// </remarks>
    /// <param name="username"> Session Glance Address</param>
    /// <param name="sessionKey"> Session key</param>
    /// <param name="screenshareView"> The screenshareView indicates to the recipient how to display the viewer.  </param>
    /// <param name="sessionProperties">Optional additional properties to send with the session invitation</param>
    void	                SendScreenshareInvitation(const GlanceString & username, const GlanceString & sessionKey, const GlanceString & screenshareView, const GlanceString & sessionProperties = GlanceString());

    /// <summary>   Shows the specified display. </summary>
    /// <param name="displayParams">   Parameters indicating which display to show, and how to show it. </param>
    void                    ShowDisplay(const DisplayParams & displayParams);

    #if __APPLE__
        #if TARGET_OS_IPHONE
        /// <summary>   Sets the list of masked views </summary>
        /// <param name="masked">   NSSet of UIView pointers</param>
        void             SetMaskingContext(MaskingContext * maskingContext);
        #else
        /// <summary>   Sets the list of masked views </summary>
        /// <param name="masked">   NSSet of NSView pointers</param>
        void                    SetMaskedViews(void * views);
        #endif
    #endif

	#ifdef WIN32
	/// <summary>   Sets a callback function that supplies a region to be masked </summary>
	void					SetMaskingCallback(MaskingCallback maskingCallback, void * context);
    #endif

    #ifdef ANDROID
        void                    ChangeDisplay(int width, int height);
    #endif

    /// <summary>   Ends the session. </summary>
    void            End();

    // Event handling
    /// <summary>   Called when an event occurs. </summary>
    /// <remarks>Subclasses should override this in order to be notified of Events.  This method will be called on a background thread. </remarks>
    /// <param name="event">   The event. </param>
    virtual void            OnEvent(const Event & event) {}

#ifndef ANDROID  // @jgg Doesn't look this these are used at all
    /// <summary>   Command listen </summary>
    void            CommandListen();  // listen for commands

    /// <summary>   Command execute. </summary>
    /// <param name="commandLine">  The command line. </param>
    void            CommandExecute(const GlanceString & commandLine); // if there is a listening instance, forward the command, else execute the command
#endif

protected:

    GlanceSession(User * u);

    virtual ~GlanceSession();
    friend class EventThread;

    GlanceSessionInternal * _gsi;
};

///
/// <summary>   Represents a connection to a screensharing session as a host. </summary>
/// 
class glanceapi HostSession : public GlanceSession
{
public:
    /// <summary>   HostSession constructor </summary>
	/// <remarks>An instance of HostSession is used to initiate a Glance screenshare session.</remarks>
	/// <param name="user">An instance of User.  User may be authenticated or anonymous.</param>
    HostSession(User * user);

    /// <summary>   Start a session to Show. </summary>
    /// <param name="startParams">Parameters controlling how the session starts</param>
    void            Show(const StartParams & startParams); 

    /// <summary>   Start a session to View. </summary>
    /// <param name="startParams">Parameters controlling how the session starts</param>
    void            View(const StartParams & startParams);

#ifdef ANDROID
    void                    ChangeDisplay(int width, int height);
#endif

};

///
/// <summary>   Represents a connection to a screensharing session as a guest. </summary>
/// 
class glanceapi GuestSession : public GlanceSession
{
public:
    /// <summary>   GuestSession constructor. </summary>
	/// <param name="user">An instance of User, or nullptr if user is anonymous.</param>
    GuestSession(User * user = NULL);

    /// <summary>   Join a session as an anonymous guest. </summary>
    /// <param name="glanceAddress">The Glance Address for the session</param>
    /// <param name="key">The session key</param>   
    /// <param name="joinParams">Parameters controlling the session join</param>
    void            Join(const GlanceString & glanceAddress, const GlanceString & key, const JoinParams & joinParams);

    /// <summary>   Join a session as an authenticated guest.  An authenticated User must have been provided to the constructor, and the User
    ///             must belong to the specified group.
    /// </summary>
    /// <param name="anonUserGroupId">The group id for the session, or zero if the group id is not known.  If the user is a member of only one group, passing 0 
    /// will join a session in the authenticated user's one and only group.</param>
    /// <param name="key">The session key</param>   
    /// <param name="joinParams">Parameters controlling the session join</param>
    void            Join(int groupId, const GlanceString & key, const JoinParams & joinParams);

	/// <summary>   Lookup information about a Glance session</summary>
	/// <param name="glanceAddress">The Glance Address for the session</param>
	/// <param name="key">The session key</param> 
	/// <returns>A SessionInfo object containing information about the session.</returns>
	SessionInfo		Lookup(const GlanceString & glanceAddress, const GlanceString & key, int retryWait = 0);

    void            SetViewerContext(ViewerContext* viewerContext);
};

} // namespace _Glance
